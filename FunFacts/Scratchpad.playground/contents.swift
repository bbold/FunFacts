// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"



var someVariable = "aVariable"

let someConstent = 20

someVariable = "anotherString"


var fruitsArray = ["apples"]

fruitsArray.append("bananas")

fruitsArray

//optionals 

var optionalString: String? = "Hello"

let bestSmartphone = "iPhone"
var favoriteSports = ["footbal", "basketball", "tennis"]



favoriteSports.append("vollyball")


var randomNumber = Int(arc4random_uniform(10))

//UIColor

var redColor = UIColor(red: 233/255.0, green: 86/255.0, blue: 94/255.0, alpha: 1.0)

let colorArray = [redColor]
